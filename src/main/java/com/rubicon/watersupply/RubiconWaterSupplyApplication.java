package com.rubicon.watersupply;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.rubicon.watersupply.filter.JwtFilter;

@SpringBootApplication
@EnableScheduling
public class RubiconWaterSupplyApplication {

	public static void main(String[] args) {
		SpringApplication.run(RubiconWaterSupplyApplication.class, args);
	}

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Bean
	public FilterRegistrationBean<JwtFilter> loggingFilter(){
	    FilterRegistrationBean<JwtFilter> registrationBean 
	      = new FilterRegistrationBean<>();
	    registrationBean.setFilter(new JwtFilter());
	    registrationBean.addUrlPatterns("/secure/*");
	    return registrationBean;    
	}
	
}
