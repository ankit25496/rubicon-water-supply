package com.rubicon.watersupply.constants;

public class Appconstants {
	
	public static final String REQUESTED="Requested";
	public static final String IN_PROGRESS="In Progress";
	public static final String DELIVERED="Delivered";
	public static final String CANCELLED ="Cancelled";
	public static final String CANCELLED_MSG= "Order was cancelled before delivery.";
	public static final String DELIVERED_MSG= "Order has been delivered.";
	public static final String IN_PROGRESS_MSG= "Order is being delivered right now.";
	public static final String REQUESTED_MSG= "Order has been placed but not yet delivered.";

	public static final String STATUS_200 ="200";
	public static final String SUCCESS ="Success";
	public static final String FAILED ="Failed";
	public static final String ALREADY_EXIST="Farmer Already Registed With given mobileNo.";
	public static final String BOOKING_ALREADY_EXIST="Booking Already ";
	public static final String STATUS_201 = "201";
	public static final String STATUS_500 = "500";
	public static final String STATUS_404 = "404";
	public static final String UNABLE_TO_PROCESS = "";
	public static final String STATUS_401 = "401";
	public static final String WRONG_USERNAME_OR_PASSWORD = "Wrong Username Or Password.";
	public static final String USER_NOT_EXIST = "User Not Exist.";
	public static final String NOT_VALID = "Not Valid.";
	public static final String CANCEL_NOT_ALLOWED = "Cancel Not Allowed As Current Status Is ";
	public static final String NOT_FOUND = "Record Not Found";
	
}
