package com.rubicon.watersupply.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.rubicon.watersupply.constants.Appconstants;
import com.rubicon.watersupply.response.Response;

@RestController
public class AuthController {
	
	@GetMapping(value = "/check")
	public ResponseEntity<Object> check() {
		return new ResponseEntity<>("Ok", HttpStatus.OK);
	}

	@RequestMapping(value = "/authentication/failed",method = { RequestMethod.GET, RequestMethod.POST })
	public ResponseEntity<Object> authenticationFailed()
	{
		Response response=new Response(false, "Invalid Token", Appconstants.STATUS_401);
		return new ResponseEntity<>(response,HttpStatus.UNAUTHORIZED);		
	}
	
	@RequestMapping(value = "/authentication/notFound",method = { RequestMethod.GET, RequestMethod.POST })
	public ResponseEntity<Object> authenticationNotFound()
	{
		Response response=new Response(false, "Token Not Found", Appconstants.STATUS_401);
		return new ResponseEntity<>(response,HttpStatus.UNAUTHORIZED);
	}
	
	@RequestMapping(value = "/authentication/deviceIdNotFound",method = { RequestMethod.GET, RequestMethod.POST })
	public ResponseEntity<Object> deviceIdNotFound()
	{
		Response response=new Response(false, "DeviceId Not Found.", Appconstants.STATUS_401);
		return new ResponseEntity<>(response,HttpStatus.UNAUTHORIZED);
	}
	
	
	@RequestMapping(value = "/authentication/misMatchDevice",method = { RequestMethod.GET, RequestMethod.POST })
	public ResponseEntity<Object> misMatchDevice()
	{
		Response response=new Response(false, "Device Mismatch", Appconstants.STATUS_401);
		return new ResponseEntity<>(response,HttpStatus.UNAUTHORIZED);
	}
	
	@RequestMapping(value = "/authentication/invalidRequest",method = { RequestMethod.GET, RequestMethod.POST })
	public ResponseEntity<Object> invalidRequest()
	{
		Response response=new Response(false, "You Are Logged In Into Different Device Loggin out...", Appconstants.STATUS_401);
		return new ResponseEntity<>(response,HttpStatus.UNAUTHORIZED);
	}
	
	@RequestMapping(value = "/pathNotFound",method = { RequestMethod.GET, RequestMethod.POST })
	public ResponseEntity<Object> pathNotFound()
	{
		Response response=new Response(false, "Path Not Found.", Appconstants.STATUS_401);
		return new ResponseEntity<>(response,HttpStatus.NOT_FOUND);
	}

}
