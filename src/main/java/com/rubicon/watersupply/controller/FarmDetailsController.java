package com.rubicon.watersupply.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rubicon.watersupply.constants.Appconstants;
import com.rubicon.watersupply.exception.CustomException;
import com.rubicon.watersupply.request.InputRequest;
import com.rubicon.watersupply.response.Response;
import com.rubicon.watersupply.service.FarmDetailsService;

@RequestMapping(value = "/secure/")
@RestController
public class FarmDetailsController {
	
	@Autowired
	FarmDetailsService farmDetailsService;
	
	@PostMapping("saveFarmDetails")
	public Response saveFarmDetails(@RequestBody InputRequest inputRequest,@RequestAttribute Long farmerId,@RequestAttribute String requestId)
	{
		try {
			inputRequest.setFarmerId(farmerId);
			inputRequest.setRequestId(requestId);
			return farmDetailsService.saveFarmDetails(inputRequest);
				
		} catch (Exception e) {
			throw new CustomException(Appconstants.UNABLE_TO_PROCESS, e.getMessage(),Boolean.FALSE
					,Appconstants.STATUS_500);}
		
	}

}
