package com.rubicon.watersupply.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.rubicon.watersupply.constants.Appconstants;
import com.rubicon.watersupply.exception.CustomException;
import com.rubicon.watersupply.request.InputRequest;
import com.rubicon.watersupply.response.Response;
import com.rubicon.watersupply.service.LoginDetailsService;

@RestController
public class LoginController {
	
	@Autowired
	LoginDetailsService loginDetailsService;
	
	@PostMapping(value = "/login")
	public Response login(@RequestHeader(defaultValue = "1234") String requestId,@RequestBody InputRequest inputRequest)
	{
		inputRequest.setRequestId(requestId);
		try {
			return loginDetailsService.login(inputRequest);		
		} catch (Exception e) {
			throw new CustomException(Appconstants.UNABLE_TO_PROCESS, e.getMessage(),Boolean.FALSE
				,Appconstants.STATUS_500);}
	}


}
