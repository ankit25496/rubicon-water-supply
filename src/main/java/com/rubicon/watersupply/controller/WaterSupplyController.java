package com.rubicon.watersupply.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rubicon.watersupply.constants.Appconstants;
import com.rubicon.watersupply.exception.CustomException;
import com.rubicon.watersupply.request.InputRequest;
import com.rubicon.watersupply.request.Request;
import com.rubicon.watersupply.request.RequestData;
import com.rubicon.watersupply.response.Response;
import com.rubicon.watersupply.service.WaterSupplyService;

@RequestMapping(value = "/secure/")
@RestController
public class WaterSupplyController {

	@Autowired
	WaterSupplyService waterSupplyService;

	@PostMapping("saveWaterSupplyDetails")
	public Response saveWaterSupplyDetails(@RequestBody InputRequest inputRequest, @RequestAttribute Long farmerId,
			@RequestAttribute String requestId) {
		try {
			inputRequest.setFarmerId(farmerId);
			inputRequest.setRequestId(requestId);
			return waterSupplyService.saveWaterSupplyDetails(inputRequest);
			
		} catch (Exception e) {
			throw new CustomException(Appconstants.UNABLE_TO_PROCESS, e.getMessage(),Boolean.FALSE
					,Appconstants.STATUS_500);}

	}

	@GetMapping("getWaterSupplyDetails")
	public Response getWaterSupplyDetails(@RequestParam(required = false) String status,@RequestAttribute Long farmerId,
			@RequestAttribute String requestId) {
		try {
			InputRequest inputRequest=new InputRequest();
			Request request=new Request();
			RequestData requestData=new RequestData();
			requestData.setStatus(status);
			inputRequest.setFarmerId(farmerId);
			inputRequest.setRequestId(requestId);
			request.setRequestData(requestData);
			inputRequest.setRequest(request);
			return waterSupplyService.getWaterSupplyDetails(inputRequest);
			
		} catch (Exception e) {
			throw new CustomException(Appconstants.UNABLE_TO_PROCESS, e.getMessage(),Boolean.FALSE
					,Appconstants.STATUS_500);}

	}
	
	@PutMapping("cancelWaterSupplyDetails")
	public Response cancelWaterSupplyDetails(@RequestAttribute Long farmerId,@RequestBody InputRequest inputRequest,
			@RequestAttribute String requestId) {
		try {
			inputRequest.setFarmerId(farmerId);
			inputRequest.setRequestId(requestId);
			return waterSupplyService.cancelWaterSupplyDetails(inputRequest);
			
		} catch (Exception e) {
			throw new CustomException(Appconstants.UNABLE_TO_PROCESS, e.getMessage(),Boolean.FALSE
					,Appconstants.STATUS_500);}

	}
}

