package com.rubicon.watersupply.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"created_by","created_on","modified_by","modified_on"}, allowGetters = true)
@JsonPropertyOrder(value = {"created_by","created_on","modified_by","modified_on"} )
public class AuditDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "created_on", nullable = true, updatable = false)
	@CreatedDate
	private LocalDateTime createdOn=LocalDateTime.now();
	
	@Column(name = "created_by",nullable = true,insertable = true,updatable = false)
	private Long createdBy;

	@Column(name = "modified_on", nullable = true,insertable = false,updatable = true)
	@UpdateTimestamp
	private LocalDateTime modifiedOn;

	@Column(name = "modified_by",nullable = true)
	private Long modifiedBy;

	public LocalDateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(LocalDateTime createdOn) {
		this.createdOn = createdOn;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public LocalDateTime getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(LocalDateTime modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	@Override
	public String toString() {
		return "AuditDetails [createdOn=" + createdOn + ", createdBy=" + createdBy + ", modifiedOn=" + modifiedOn
				+ ", modifiedBy=" + modifiedBy + "]";
	}
	
	
	
}
