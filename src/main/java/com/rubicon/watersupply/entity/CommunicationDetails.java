package com.rubicon.watersupply.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "communication_details")
public class CommunicationDetails extends AuditDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long communicationId;
	private String mobileNo;
	private String email;
	private boolean isActive;
	public Long getCommunicationId() {
		return communicationId;
	}
	public void setCommunicationId(Long communicationId) {
		this.communicationId = communicationId;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	@Override
	public String toString() {
		return "CommunicationDetails [communicationId=" + communicationId + ", mobileNo=" + mobileNo + ", email="
				+ email + ", isActive=" + isActive + "]";
	}
}
