package com.rubicon.watersupply.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "farmer_details")
public class FarmerDetails extends AuditDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long farmerId;
	private String firstName;
	private String lastName;
	private boolean isActive;
	private String profileImage;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, optional = false)
	@JoinColumn(name = "communicationId", nullable = false)
	private CommunicationDetails communication;
	
	
	
	public FarmerDetails(Long farmerId) {
		this.farmerId=farmerId;
	}

	public CommunicationDetails getCommunication() {
		return communication;
	}

	public void setCommunication(CommunicationDetails communication) {
		this.communication = communication;
	}

	public String getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}

	public Long getFarmerId() {
		return farmerId;
	}

	public void setFarmerId(Long farmerId) {
		this.farmerId = farmerId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	@Override
	public String toString() {
		return "FarmerDetails [farmerId=" + farmerId + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", isActive=" + isActive + ", profileImage=" + profileImage + "]";
	}

}
