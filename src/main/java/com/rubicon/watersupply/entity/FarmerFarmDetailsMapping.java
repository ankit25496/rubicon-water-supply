package com.rubicon.watersupply.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@Table(name="farmer_farm_details_mapping")
public class FarmerFarmDetailsMapping extends AuditDetails {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long mappingId;
	
	private boolean isActive;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "farmerId")
	@Cascade({CascadeType.SAVE_UPDATE})
    private FarmerDetails farmer;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "farmId", referencedColumnName = "farmId")
	@Cascade({CascadeType.SAVE_UPDATE})
	private FarmDetails farm;



	public FarmerFarmDetailsMapping(Long mappingId) {
		this.mappingId=mappingId;
	}

	public FarmerFarmDetailsMapping() {
	super();
	}

	public Long getMappingId() {
		return mappingId;
	}

	public void setMappingId(Long mappingId) {
		this.mappingId = mappingId;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public FarmerDetails getFarmer() {
		return farmer;
	}

	public void setFarmer(FarmerDetails farmer) {
		this.farmer = farmer;
	}

	public FarmDetails getFarm() {
		return farm;
	}

	public void setFarm(FarmDetails farm) {
		this.farm = farm;
	}

	@Override
	public String toString() {
		return "FarmerFarmDetailsMapping [mappingId=" + mappingId + ", isActive=" + isActive + ", farmer=" + farmer
				+ ", farm=" + farm + "]";
	}
	

}
