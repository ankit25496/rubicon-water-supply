package com.rubicon.watersupply.entity;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "login_details")
public class LoginDetails extends AuditDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long loginId;
	
	private String password;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, optional = false)
	@JoinColumn(name = "farmer_id", nullable = false)
	private FarmerDetails farmer;
	private LocalDateTime lastLogin;
	private boolean changePassword;
	private boolean isLoginBlocked;
	public Long getLoginId() {
		return loginId;
	}
	public void setLoginId(Long loginId) {
		this.loginId = loginId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public FarmerDetails getFarmer() {
		return farmer;
	}
	public void setFarmer(FarmerDetails farmer) {
		this.farmer = farmer;
	}
	public LocalDateTime getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(LocalDateTime lastLogin) {
		this.lastLogin = lastLogin;
	}
	public boolean isChangePassword() {
		return changePassword;
	}
	public void setChangePassword(boolean changePassword) {
		this.changePassword = changePassword;
	}
	public boolean isLoginBlocked() {
		return isLoginBlocked;
	}
	public void setLoginBlocked(boolean isLoginBlocked) {
		this.isLoginBlocked = isLoginBlocked;
	}
	@Override
	public String toString() {
		return "LoginDetails [loginId=" + loginId + ", password=" + password + ", farmer=" + farmer + ", lastLogin="
				+ lastLogin + ", changePassword=" + changePassword + ", isLoginBlocked=" + isLoginBlocked + "]";
	}
	
	
	
	
}
