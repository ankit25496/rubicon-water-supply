package com.rubicon.watersupply.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "water_supply_details")
public class WaterSupplyDetails extends AuditDetails {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long supplyId;
	
	 @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private Date supplyStartDateTime;
	private Date supplyEndDateTime;
	private long duration;
	private String status;
	private boolean isActive;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "mappingId", referencedColumnName = "mappingId")
	@Cascade({CascadeType.SAVE_UPDATE})
	private FarmerFarmDetailsMapping farmerFarmDetails;
	private String remarks;

	
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getSupplyId() {
		return supplyId;
	}

	public void setSupplyId(Long supplyId) {
		this.supplyId = supplyId;
	}

	public Date getSupplyStartDateTime() {
		return supplyStartDateTime;
	}

	public void setSupplyStartDateTime(Date supplyStartDateTime) {
		this.supplyStartDateTime = supplyStartDateTime;
	}

	public Date getSupplyEndDateTime() {
		return supplyEndDateTime;
	}

	public void setSupplyEndDateTime(Date supplyEndDateTime) {
		this.supplyEndDateTime = supplyEndDateTime;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public FarmerFarmDetailsMapping getFarmerFarmDetails() {
		return farmerFarmDetails;
	}

	public void setFarmerFarmDetails(FarmerFarmDetailsMapping farmerFarmDetails) {
		this.farmerFarmDetails = farmerFarmDetails;
	}

	@Override
	public String toString() {
		return "WaterSupplyDetails [supplyId=" + supplyId + ", supplyStartDateTime=" + supplyStartDateTime
				+ ", supplyEndDateTime=" + supplyEndDateTime + ", duration=" + duration + ", status=" + status
				+ ", isActive=" + isActive + ", farmerFarmDetails=" + farmerFarmDetails + "]";
	}
	
}
