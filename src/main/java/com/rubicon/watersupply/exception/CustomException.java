package com.rubicon.watersupply.exception;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.rubicon.watersupply.response.Response;
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomException extends RuntimeException {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;
	private String message;
	private String details;
	private Response response;
	private Boolean status;
	private String statusCode;

	protected CustomException() {
	}

	public CustomException(String message, String details, Response response, Boolean status, String statusCode) {
		this.message = message;
		this.details = details;
		this.response = response;
		this.status = status;
		this.statusCode = statusCode;
	}

	public CustomException(String message, String details, Boolean status, String statusCode) {
		super();
		this.message = message;
		this.details = details;
		this.status = status;
		this.statusCode = statusCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	@Override
	public String toString() {
		return "CustomException [message=" + message + ", details=" + details + ", response=" + response + ", status="
				+ status + ", statusCode=" + statusCode + "]";
	}

}