package com.rubicon.watersupply.filter;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;
import com.rubicon.watersupply.utils.TokenEncryptionUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;

@Component
public class JwtFilter extends GenericFilterBean {
	
	@Value("${jwt.secret}")
	private String secret="1234";

	@Override
	public void doFilter(final ServletRequest req, final ServletResponse res, final FilterChain chain)
			throws IOException, ServletException, SignatureException {
		
		TokenEncryptionUtils tokenEncryptionUtils=new TokenEncryptionUtils("");
		RequestDispatcher invalidToken = req.getRequestDispatcher("/authentication/failed");
		RequestDispatcher tokenNotFound = req.getRequestDispatcher("/authentication/notFound");
			
		final HttpServletRequest request = (HttpServletRequest) req;
		final HttpServletResponse response = (HttpServletResponse) res;
		String authHeader = request.getHeader("authorization");
		authHeader=tokenEncryptionUtils.decrypt(authHeader);  
		
		if ("OPTIONS".equals(request.getMethod())) {
			response.setStatus(HttpServletResponse.SC_OK);
			chain.doFilter(req, res);
		} else {

			if (authHeader.isEmpty() || !authHeader.startsWith("Bearer ")) {
				tokenNotFound.forward(req, res);
			}

			try {
				final String token = authHeader.substring(7);
				final Claims claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();	
				Integer farmerId=(Integer) claims.get("farmerId");
				request.setAttribute("claims", claims);
				UUID uuid = UUID.randomUUID();
		        String requestId = uuid.toString();
				request.setAttribute("farmerId", farmerId);
				request.setAttribute("requestId", requestId);
				chain.doFilter(request, response);		
			} catch (Exception e) {
				e.printStackTrace();
				invalidToken.forward(req, res);

			}

		}
	}

}