package com.rubicon.watersupply.helper;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public interface WaterSupplyHelper {
	
	@JsonProperty("farmName")
	public String getName();
	@JsonProperty("farmerName")
	public String getFirstName();
	public Long getFarmId();
	public Long getFarmerId();
	public double getArea();
	public String getStatus();
	public Date getSupplyStartDateTime();
	public Date getSupplyEndDateTime();
	public Long getSupplyId();

}
