package com.rubicon.watersupply.jwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.rubicon.watersupply.constants.Appconstants;
import com.rubicon.watersupply.exception.CustomException;
import com.rubicon.watersupply.utils.MetaDataDetails;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;

@Service
public class JwtTokenDecryptor {
	
	@Autowired
	JwtTokenUtil jwtTokenUtil;

    public MetaDataDetails parseToken(String token) {
        try {
        	
        	if(token!=null  && token.startsWith("Bearer "))
        	{
        		token = token.substring(7);
            	Claims body=jwtTokenUtil.getAllClaimsFromToken(token);
                String mobileNo=(String) body.get("mobileNo");
                Long farmerId=(Long) body.get("farmerId");
                String firstName=(String) body.get("firstName");
                String lastName=(String) body.get("lastName");
                return new MetaDataDetails(firstName, lastName, farmerId, mobileNo);	
        	}
        	else
        	{
        	     throw new CustomException(Appconstants.NOT_VALID, "Not a valid token", Boolean.FALSE,
                         Appconstants.STATUS_401);
        	}
        	
        } catch (JwtException | ClassCastException e) {
            throw new CustomException(Appconstants.UNABLE_TO_PROCESS, e.getMessage(), Boolean.FALSE,
            		Appconstants.STATUS_401);
   	}
    }

}
