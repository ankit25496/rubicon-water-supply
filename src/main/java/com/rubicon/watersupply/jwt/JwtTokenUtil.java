package com.rubicon.watersupply.jwt;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.rubicon.watersupply.utils.MetaDataDetails;
import com.rubicon.watersupply.utils.TokenEncryptionUtils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtTokenUtil implements Serializable {

	private static final long serialVersionUID = -2550185165626007488L;

	public static final long JWT_TOKEN_VALIDITY =  2592000;  //30 days

	@Value("${jwt.secret}")
	private String secret;
	
	//retrieve username from jwt token
	public String getUsernameFromToken(String token) {
		return getClaimFromToken(token, Claims::getSubject);
	}

	//retrieve expiration date from jwt token
	public Date getExpirationDateFromToken(String token) {
		return getClaimFromToken(token, Claims::getExpiration);
	}

	public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
		final Claims claims = getAllClaimsFromToken(token);
		return claimsResolver.apply(claims);
	}
    //for retrieveing any information from token we will need the secret key
	public Claims getAllClaimsFromToken(String token) {
		return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
	}

	//check if the token has expired
	private Boolean isTokenExpired(String token) {
		final Date expiration = getExpirationDateFromToken(token);
		return expiration.before(new Date());
	}

	//generate token for user
	public String generateToken( MetaDataDetails metaDataDetails) {
		Map<String, Object> claims = new HashMap<>();
		String userName="";
		claims.put("firstName", metaDataDetails.getFirstName());
		claims.put("lastName", metaDataDetails.getLastName());
		claims.put("farmerId", metaDataDetails.getFarmerId());
		claims.put("mobileNo", metaDataDetails.getMobileNo());
		return doGenerateToken(claims,userName);
	}

	private String doGenerateToken(Map<String, Object> claims, String subject) {
		String token= "Bearer "+Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 1000))
				.signWith(SignatureAlgorithm.HS512, secret).compact();
		 TokenEncryptionUtils encrypter = new TokenEncryptionUtils("");
		token=encrypter.encrypt(token);
		return token;
	}

	//validate token
	public Boolean validateToken(String token, MetaDataDetails metaDataDetails) {
		final String usernamefromToken = getUsernameFromToken(token);
		String username=metaDataDetails.getMobileNo();
		return (username.equals(usernamefromToken) && !isTokenExpired(token));
	}
}