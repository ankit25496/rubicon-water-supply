package com.rubicon.watersupply.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.rubicon.watersupply.entity.CommunicationDetails;

@Repository
public interface CommunicationRepository extends JpaRepository<CommunicationDetails,Long> {

	boolean existsByMobileNo(String mobileNo);

}
