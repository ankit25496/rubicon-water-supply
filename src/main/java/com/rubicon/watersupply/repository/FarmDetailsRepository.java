package com.rubicon.watersupply.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rubicon.watersupply.entity.FarmDetails;

@Repository
public interface FarmDetailsRepository extends JpaRepository<FarmDetails,Long> {

}
