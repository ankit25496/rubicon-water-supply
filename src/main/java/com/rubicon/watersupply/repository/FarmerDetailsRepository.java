package com.rubicon.watersupply.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.rubicon.watersupply.entity.FarmerDetails;

@Repository
public interface FarmerDetailsRepository extends JpaRepository<FarmerDetails,Long> {

}
