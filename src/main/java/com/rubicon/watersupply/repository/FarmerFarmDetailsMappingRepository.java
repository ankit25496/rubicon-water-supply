package com.rubicon.watersupply.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.rubicon.watersupply.entity.FarmerFarmDetailsMapping;

@Repository
public interface FarmerFarmDetailsMappingRepository extends JpaRepository<FarmerFarmDetailsMapping,Long> {

	@Query(value = "select mapping_id from farmer_farm_details_mapping ffdm where ffdm.farm_id=?2 and ffdm.farmer_id=?1 and ffdm.is_active=true",nativeQuery = true)
	Long getMappingId(Long farmerId, Long farmId);

}
