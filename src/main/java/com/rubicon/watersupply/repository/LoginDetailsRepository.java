package com.rubicon.watersupply.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.rubicon.watersupply.entity.LoginDetails;

@Repository
public interface LoginDetailsRepository extends JpaRepository<LoginDetails,Long> {

	LoginDetails findByFarmerCommunicationMobileNo(String username);

	

}
