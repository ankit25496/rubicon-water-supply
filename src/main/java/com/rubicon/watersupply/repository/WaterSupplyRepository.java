package com.rubicon.watersupply.repository;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.rubicon.watersupply.entity.WaterSupplyDetails;
import com.rubicon.watersupply.helper.WaterSupplyHelper;

@Repository
public interface WaterSupplyRepository extends JpaRepository<WaterSupplyDetails,Long> {

	
	@Query(value = "select * from water_supply_details wsd left join farmer_farm_details_mapping ffdm ON wsd.mapping_id = ffdm.mapping_id where ffdm.farm_id =?1 and  status !='Completed' and ?2 between  supply_start_date_time  and supply_end_date_time ",nativeQuery = true)
	WaterSupplyDetails existByFarmId(Long farmId, Date supplyStartDateTime);
	
	@Query(value = "select fd2.name,fd2.farm_id as farmid,fd.first_name as firstname,fd.farmer_id as farmerid ,fd2.area,wsd.status,wsd.supply_start_date_time as supplystartdatetime,wsd.supply_end_date_time as supplyenddatetime ,wsd.supply_id as supplyId from water_supply_details wsd left join farmer_farm_details_mapping ffdm ON wsd.mapping_id = ffdm.mapping_id left join farmer_details fd on ffdm.farmer_id =fd.farmer_id left join farm_details fd2 on ffdm.farm_id =fd2.farm_id where fd.farmer_id =?1",nativeQuery = true)
	List<WaterSupplyHelper>getWaterSupplyDetailsBasedOnFarmerId(Long farmerId);
	
	@Query(value = "select fd2.name,fd2.farm_id as farmid,fd.first_name as firstname,fd.farmer_id as farmerid ,fd2.area,wsd.status,wsd.supply_start_date_time as supplystartdatetime,wsd.supply_end_date_time as supplyenddatetime,wsd.supply_id as supplyId from water_supply_details wsd left join farmer_farm_details_mapping ffdm ON wsd.mapping_id = ffdm.mapping_id left join farmer_details fd on ffdm.farmer_id =fd.farmer_id left join farm_details fd2 on ffdm.farm_id =fd2.farm_id where fd.farmer_id =?1 and status =?2",nativeQuery = true)
	List<WaterSupplyHelper>getWaterSupplyDetailsBasedOnStatus(Long farmerId,String status);

	WaterSupplyDetails findByFarmerFarmDetailsFarmFarmIdAndStatusNot(Long farmerId, String status);


	@Query(value = "select fd2.name,fd2.farm_id as farmId,wsd.status,wsd.supply_start_date_time as supplystartdatetime,wsd.supply_end_date_time as supplyenddatetime ,wsd.supply_id as supplyId from water_supply_details wsd left join farmer_farm_details_mapping ffdm ON wsd.mapping_id = ffdm.mapping_id left join farm_details fd2 on ffdm.farm_id =fd2.farm_id where wsd.status not in ('Completed','Delivered','Cancelled')",nativeQuery = true)
	List<WaterSupplyHelper>getAllWaterSupplyDetails();

	@Modifying
	@Transactional
	@Query(value = "update WaterSupplyDetails set status=?2,modifiedOn=?3,modifiedBy='schedular'  where supplyId=?1")
	int updateStatus(Long supplyId, String inProgress,LocalDateTime currentdateTime);
}
