package com.rubicon.watersupply.request;

public class InputRequest {
	
	private String requestId;
	private Request request;
	private Long farmerId;
	
	
	public Long getFarmerId() {
		return farmerId;
	}
	public void setFarmerId(Long farmerId) {
		this.farmerId = farmerId;
	}
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public Request getRequest() {
		return request;
	}
	public void setRequest(Request request) {
		this.request = request;
	}
	@Override
	public String toString() {
		return "InputRequest [requestId=" + requestId + ", request=" + request + "]";
	}
	
	

}
