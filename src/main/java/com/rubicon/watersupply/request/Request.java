package com.rubicon.watersupply.request;

public class Request {
	
	private RequestData requestData;

	public RequestData getRequestData() {
		return requestData;
	}

	public void setRequestData(RequestData requestData) {
		this.requestData = requestData;
	}
	

}
