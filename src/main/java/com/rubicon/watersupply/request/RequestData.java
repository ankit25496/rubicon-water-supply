package com.rubicon.watersupply.request;

import com.rubicon.watersupply.entity.FarmDetails;
import com.rubicon.watersupply.entity.FarmerDetails;
import com.rubicon.watersupply.entity.WaterSupplyDetails;

public class RequestData {
	
	private FarmerDetails farmer;
	private String username;
	private String password;
	private FarmDetails farm;
	private WaterSupplyDetails waterSupplyDetails;
	private String status;
	
	
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public WaterSupplyDetails getWaterSupplyDetails() {
		return waterSupplyDetails;
	}

	public void setWaterSupplyDetails(WaterSupplyDetails waterSupplyDetails) {
		this.waterSupplyDetails = waterSupplyDetails;
	}

	public FarmDetails getFarm() {
		return farm;
	}

	public void setFarm(FarmDetails farm) {
		this.farm = farm;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public FarmerDetails getFarmer() {
		return farmer;
	}

	public void setFarmer(FarmerDetails farmer) {
		this.farmer = farmer;
	}
	
	

}
