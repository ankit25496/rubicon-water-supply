package com.rubicon.watersupply.response;

public class Response {
	
	private boolean status;
	private String statusCode;
	private String message;
	private ResponseObject responseObject;
	
	public Response() {
		super();
	}
	public Response(boolean b, String message, String statusCode) {
	this.status=b;
	this.message=message;
	this.statusCode=statusCode;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
	public ResponseObject getResponseObject() {
		return responseObject;
	}
	public void setResponseObject(ResponseObject responseObject) {
		this.responseObject = responseObject;
	}
	@Override
	public String toString() {
		return "Response [status=" + status + ", statusCode=" + statusCode + ", message=" + message + "]";
	}
	
	

}
