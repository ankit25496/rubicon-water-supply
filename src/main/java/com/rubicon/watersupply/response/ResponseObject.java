package com.rubicon.watersupply.response;

import java.util.List;

import com.rubicon.watersupply.helper.WaterSupplyHelper;
import com.rubicon.watersupply.utils.MetaDataDetails;

public class ResponseObject {
	private String token;
	private MetaDataDetails metaDataDetails;
	private List<WaterSupplyHelper> waterSupplyDetails;
	
	
	public List<WaterSupplyHelper> getWaterSupplyDetails() {
		return waterSupplyDetails;
	}

	public void setWaterSupplyDetails(List<WaterSupplyHelper> waterSupplyDetails) {
		this.waterSupplyDetails = waterSupplyDetails;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public MetaDataDetails getMetaDataDetails() {
		return metaDataDetails;
	}

	public void setMetaDataDetails(MetaDataDetails metaDataDetails) {
		this.metaDataDetails = metaDataDetails;
	}

	@Override
	public String toString() {
		return "ResponseObject [token=" + token + ", metaDataDetails=" + metaDataDetails + "]";
	}

}
