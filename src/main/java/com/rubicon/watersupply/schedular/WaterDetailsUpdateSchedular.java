package com.rubicon.watersupply.schedular;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.rubicon.watersupply.constants.Appconstants;
import com.rubicon.watersupply.helper.WaterSupplyHelper;
import com.rubicon.watersupply.repository.WaterSupplyRepository;

@Component
public class WaterDetailsUpdateSchedular {

	private static Logger logger = LoggerFactory.getLogger(WaterDetailsUpdateSchedular.class);
    
	@Autowired
	WaterSupplyRepository waterSupplyRepository;
	
	
	@Scheduled(cron="0 0/10 * * * ?")
	private void checkVehicleSyncStatus()
	{
		logger.info("WaterDetailsUpdateSchedular run on date {} ",new Date());
		List<WaterSupplyHelper> waterSupplyHelpers=waterSupplyRepository.getAllWaterSupplyDetails();
	    for (WaterSupplyHelper waterSupplyHelper : waterSupplyHelpers) {
	    	String currentStatus=waterSupplyHelper.getStatus();
	    	Date supplyStartDate=waterSupplyHelper.getSupplyStartDateTime();
	    	Date supplyEndDate=waterSupplyHelper.getSupplyEndDateTime();
	    	if(Appconstants.REQUESTED.equalsIgnoreCase(currentStatus))
	    	{
	    		boolean betweenStatus=isDateInBetweenIncludingEndPoints(supplyStartDate,supplyEndDate);
	    		if(betweenStatus)
	    		{
	    			logger.info("Water delivery to farm {} started.",waterSupplyHelper.getFarmId());
	    			waterSupplyRepository.updateStatus(waterSupplyHelper.getSupplyId(),Appconstants.IN_PROGRESS,LocalDateTime.now());
	    		}
	    	}
	    	else if(Appconstants.IN_PROGRESS.equalsIgnoreCase(currentStatus))
	    	{
	    		long diff = supplyEndDate.getTime() - new Date().getTime();
	    		long diffMinutes = diff / (60 * 1000) % 60; 
	    		long diffHours = diff / (60 * 60 * 1000);
	    		long diffDays = diff / (60 * 60 * 1000 * 24);
	    		if(diffDays==0 && diffHours==0 && diffMinutes <=5)
	    		{
	    			logger.info("Water delivery to farm {} stopped.",waterSupplyHelper.getFarmId());
	    			waterSupplyRepository.updateStatus(waterSupplyHelper.getSupplyId(),Appconstants.DELIVERED,LocalDateTime.now());
		    		
	    		}
	    	}
			
		}
		 
	}

	
	public  boolean isDateInBetweenIncludingEndPoints(final Date min, final Date max){
		Date date=new Date();
	    return !(date.before(min) || date.after(max));
	}
}
