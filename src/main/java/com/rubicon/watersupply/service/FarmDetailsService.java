package com.rubicon.watersupply.service;

import com.rubicon.watersupply.request.InputRequest;
import com.rubicon.watersupply.response.Response;

public interface FarmDetailsService {
	
	public Response saveFarmDetails(InputRequest inputRequest);

}
