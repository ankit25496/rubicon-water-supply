package com.rubicon.watersupply.service;

import com.rubicon.watersupply.request.InputRequest;
import com.rubicon.watersupply.response.Response;

public interface FarmerDetailsService {
	
	public Response saveFarmerDetails(InputRequest inputRequest);
}
