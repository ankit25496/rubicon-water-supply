package com.rubicon.watersupply.service;

import com.rubicon.watersupply.entity.FarmerDetails;
import com.rubicon.watersupply.request.InputRequest;
import com.rubicon.watersupply.response.Response;

public interface LoginDetailsService {
	
	public boolean createFarmerLogin(FarmerDetails farmerDetails);
	public Response login(InputRequest inputRequest);

}
