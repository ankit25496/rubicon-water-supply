package com.rubicon.watersupply.service;

import com.rubicon.watersupply.request.InputRequest;
import com.rubicon.watersupply.response.Response;

public interface WaterSupplyService {

	public Response saveWaterSupplyDetails(InputRequest inputRequest);

	public Response getWaterSupplyDetails(InputRequest inputRequest);

	public Response cancelWaterSupplyDetails(InputRequest inputRequest);
}
