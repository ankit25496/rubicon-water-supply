package com.rubicon.watersupply.serviceimpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.rubicon.watersupply.constants.Appconstants;
import com.rubicon.watersupply.entity.FarmDetails;
import com.rubicon.watersupply.entity.FarmerDetails;
import com.rubicon.watersupply.entity.FarmerFarmDetailsMapping;
import com.rubicon.watersupply.repository.FarmDetailsRepository;
import com.rubicon.watersupply.repository.FarmerFarmDetailsMappingRepository;
import com.rubicon.watersupply.request.InputRequest;
import com.rubicon.watersupply.request.RequestData;
import com.rubicon.watersupply.response.Response;
import com.rubicon.watersupply.service.FarmDetailsService;

@Service
public class FarmDetailsServiceImpl implements FarmDetailsService {
	
	private static Logger logger = LoggerFactory.getLogger(FarmDetailsServiceImpl.class);
	
	
	@Autowired
	FarmDetailsRepository farmDetailsRepository;
	
	@Autowired
	FarmerFarmDetailsMappingRepository farmDetailsMappingRepository;

	@Override
	public Response saveFarmDetails(InputRequest inputRequest) {
		String requestId=inputRequest.getRequestId();
		logger.info("for requestId {} save farm details is called",requestId);
		Long farmerId=inputRequest.getFarmerId();
		RequestData requestData=inputRequest.getRequest().getRequestData();
		Response response=new Response();
		FarmDetails farmDetails=requestData.getFarm();
		farmDetails.setCreatedBy(farmerId);
		farmDetails.setActive(Boolean.TRUE);
		try {
			farmDetailsRepository.save(farmDetails);
			FarmerFarmDetailsMapping farmerFarmDetailsMapping=new FarmerFarmDetailsMapping();
			farmerFarmDetailsMapping.setCreatedBy(farmerId);
			farmerFarmDetailsMapping.setActive(Boolean.TRUE);
			farmerFarmDetailsMapping.setFarm(farmDetails);
			farmerFarmDetailsMapping.setFarmer(new FarmerDetails(farmerId));
			farmDetailsMappingRepository.save(farmerFarmDetailsMapping);
			response.setMessage(Appconstants.SUCCESS);
			response.setStatus(Boolean.TRUE);
			response.setStatusCode(Appconstants.STATUS_200);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return response;
	}

}
