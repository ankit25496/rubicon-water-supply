package com.rubicon.watersupply.serviceimpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rubicon.watersupply.constants.Appconstants;
import com.rubicon.watersupply.entity.CommunicationDetails;
import com.rubicon.watersupply.entity.FarmerDetails;
import com.rubicon.watersupply.repository.CommunicationRepository;
import com.rubicon.watersupply.repository.FarmerDetailsRepository;
import com.rubicon.watersupply.request.InputRequest;
import com.rubicon.watersupply.request.RequestData;
import com.rubicon.watersupply.response.Response;
import com.rubicon.watersupply.service.FarmerDetailsService;
import com.rubicon.watersupply.service.LoginDetailsService;

@Service
public class FarmerDetailsServiceImpl implements FarmerDetailsService {
	
	private static Logger logger = LoggerFactory.getLogger(FarmerDetailsServiceImpl.class);
	
	@Autowired
	FarmerDetailsRepository farmerDetailsRepository;
	
	@Autowired
	LoginDetailsService loginDetailsService;
	
	@Autowired
	CommunicationRepository communicationRepository;


	@Override
	public Response saveFarmerDetails(InputRequest inputRequest) {
		String requestId = inputRequest.getRequestId();
		logger.info("for requestId {} saveFarmerDetails is called", requestId);
		RequestData requestData = inputRequest.getRequest().getRequestData();
		CommunicationDetails communicationDetails = requestData.getFarmer().getCommunication();
		FarmerDetails farmerDetails = requestData.getFarmer();
		Response response = new Response();
		try {
			boolean mobileExistStatus = communicationRepository.existsByMobileNo(communicationDetails.getMobileNo());
			if (!mobileExistStatus) {
				communicationDetails.setActive(Boolean.TRUE);
				communicationDetails.setCreatedBy(1L);
				communicationDetails = communicationRepository.save(communicationDetails);
				farmerDetails.setCommunication(communicationDetails);
				farmerDetails.setActive(Boolean.TRUE);
				farmerDetails.setCreatedBy(1L);
				farmerDetailsRepository.save(farmerDetails);
				boolean loginStatus = loginDetailsService.createFarmerLogin(farmerDetails);
				if (loginStatus) {
					response.setMessage(Appconstants.SUCCESS);
					response.setStatus(Boolean.TRUE);
					response.setStatusCode(Appconstants.STATUS_200);
				}
			} else {
				response.setMessage(Appconstants.ALREADY_EXIST);
				response.setStatus(Boolean.FALSE);
				response.setStatusCode(Appconstants.STATUS_200);
			}
		} catch (Exception e) {
			logger.error("for requestId {} error occured while saveFarmerDetails {}", requestId, e);
		}

		return response;
	}

}
