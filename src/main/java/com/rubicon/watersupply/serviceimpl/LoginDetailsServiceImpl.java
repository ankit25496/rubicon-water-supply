package com.rubicon.watersupply.serviceimpl;

import java.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import com.rubicon.watersupply.constants.Appconstants;
import com.rubicon.watersupply.entity.FarmerDetails;
import com.rubicon.watersupply.entity.LoginDetails;
import com.rubicon.watersupply.jwt.JwtTokenUtil;
import com.rubicon.watersupply.repository.LoginDetailsRepository;
import com.rubicon.watersupply.request.InputRequest;
import com.rubicon.watersupply.request.RequestData;
import com.rubicon.watersupply.response.Response;
import com.rubicon.watersupply.response.ResponseObject;
import com.rubicon.watersupply.service.LoginDetailsService;
import com.rubicon.watersupply.utils.MetaDataDetails;

@Service
public class LoginDetailsServiceImpl implements LoginDetailsService {
	
	private static Logger logger = LoggerFactory.getLogger(LoginDetailsServiceImpl.class);
	
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	LoginDetailsRepository loginDetailsRepository;
	
	@Autowired
	JwtTokenUtil jwtTokenUtil;

	@Override
	public boolean createFarmerLogin(FarmerDetails farmerDetails) {
		LoginDetails loginDetails=new LoginDetails();
		loginDetails.setChangePassword(Boolean.FALSE);
		loginDetails.setCreatedBy(farmerDetails.getFarmerId());
		loginDetails.setCreatedOn(LocalDateTime.now());
		loginDetails.setLoginBlocked(Boolean.FALSE);
		String encryptedPassword=bCryptPasswordEncoder.encode(farmerDetails.getCommunication().getMobileNo());
		loginDetails.setPassword(encryptedPassword);
		loginDetails.setFarmer(farmerDetails);
		loginDetailsRepository.save(loginDetails);
		if(loginDetails.getLoginId()!=null)
		{
			return true;
		}
		else
		{
		return false;
		}
	}

	@Override
	public Response login(InputRequest inputRequest) {
		String requestId = inputRequest.getRequestId();
		logger.info("for requestId {} login",requestId);
		RequestData requestData = inputRequest.getRequest().getRequestData();
		LoginDetails loginDetails = loginDetailsRepository.findByFarmerCommunicationMobileNo(requestData.getUsername());
		Response response = new Response();
		if (loginDetails != null) {
			String encodedPassword = loginDetails.getPassword();
			boolean passwordMatchStatus = bCryptPasswordEncoder.matches(requestData.getPassword(), encodedPassword);
			if (passwordMatchStatus) {
				MetaDataDetails metaDataDetails=new MetaDataDetails(loginDetails.getFarmer().getFirstName(), loginDetails.getFarmer().getLastName(), loginDetails.getFarmer().getFarmerId(), loginDetails.getFarmer().getCommunication().getMobileNo());
				String token=jwtTokenUtil.generateToken(metaDataDetails);
				ResponseObject responseObject=new ResponseObject();
				responseObject.setMetaDataDetails(metaDataDetails);
				responseObject.setToken(token);
				response.setResponseObject(responseObject);
				response.setMessage(Appconstants.SUCCESS);
				response.setStatus(Boolean.TRUE);
				response.setStatusCode(Appconstants.STATUS_200);
			} else {
				response.setMessage(Appconstants.WRONG_USERNAME_OR_PASSWORD);
				response.setStatus(Boolean.FALSE);
				response.setStatusCode(Appconstants.STATUS_401);

			}
		} else {
			response.setMessage(Appconstants.USER_NOT_EXIST);
			response.setStatus(Boolean.FALSE);
			response.setStatusCode(Appconstants.STATUS_404);
		}
		return response;
	}

}
