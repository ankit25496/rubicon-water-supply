package com.rubicon.watersupply.serviceimpl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.rubicon.watersupply.constants.Appconstants;
import com.rubicon.watersupply.entity.FarmerFarmDetailsMapping;
import com.rubicon.watersupply.entity.WaterSupplyDetails;
import com.rubicon.watersupply.helper.WaterSupplyHelper;
import com.rubicon.watersupply.repository.FarmerFarmDetailsMappingRepository;
import com.rubicon.watersupply.repository.WaterSupplyRepository;
import com.rubicon.watersupply.request.InputRequest;
import com.rubicon.watersupply.request.RequestData;
import com.rubicon.watersupply.response.Response;
import com.rubicon.watersupply.response.ResponseObject;
import com.rubicon.watersupply.service.WaterSupplyService;

@Service
public class WaterSupplyServiceImpl implements WaterSupplyService {
	
	private static Logger logger = LoggerFactory.getLogger(WaterSupplyServiceImpl.class);
	
	@Autowired
	WaterSupplyRepository waterSupplyRepository;
	
	@Autowired
	FarmerFarmDetailsMappingRepository farmDetailsMappingRepository;

	@Override
	public Response saveWaterSupplyDetails(InputRequest inputRequest) {
		String requestId=inputRequest.getRequestId();
		logger.info("for requestId {} save farm details is called",requestId);
		Long farmerId=inputRequest.getFarmerId();
		RequestData requestData=inputRequest.getRequest().getRequestData();
		Response response=new Response();
		WaterSupplyDetails waterSupplyDetails=requestData.getWaterSupplyDetails();
		long duration=waterSupplyDetails.getDuration();
		Date supplyStartDateTime=waterSupplyDetails.getSupplyStartDateTime();
		Date supplyEndDateTime=addHoursToJavaUtilDate(supplyStartDateTime,(int)duration);
		try {
			WaterSupplyDetails waterSupplyDetailsDb= waterSupplyRepository.existByFarmId(requestData.getFarm().getFarmId(),supplyStartDateTime);
			if(waterSupplyDetailsDb!=null)
			{
				response.setMessage(Appconstants.BOOKING_ALREADY_EXIST.concat(waterSupplyDetailsDb.getStatus()));
				response.setStatus(Boolean.FALSE);
				response.setStatusCode(Appconstants.STATUS_200);
			}
			else
			{
				Long mappingId = farmDetailsMappingRepository.getMappingId(farmerId, requestData.getFarm().getFarmId());
				if (mappingId == null) {
					response.setMessage("No Farm Found.");
					response.setStatus(Boolean.FALSE);
					response.setStatusCode(Appconstants.STATUS_404);
				} else {
					waterSupplyDetails.setCreatedBy(farmerId);
					waterSupplyDetails.setSupplyEndDateTime(supplyEndDateTime);
					waterSupplyDetails.setStatus(Appconstants.REQUESTED);
					waterSupplyDetails.setRemarks(Appconstants.REQUESTED_MSG);
					waterSupplyDetails.setActive(Boolean.TRUE);
					waterSupplyDetails.setFarmerFarmDetails(new FarmerFarmDetailsMapping(mappingId));
					waterSupplyRepository.save(waterSupplyDetails);
					response.setMessage(Appconstants.SUCCESS);
					response.setStatus(Boolean.TRUE);
					response.setStatusCode(Appconstants.STATUS_200);

				}

			}
			
		} catch (Exception e) {
			logger.error("for requestId {} error occured in  farm details {} ",requestId,e);	
			e.printStackTrace();
		}
		
		return response;
	}

	public Date addHoursToJavaUtilDate(Date date, int hours) {
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTime(date);
	    calendar.add(Calendar.HOUR_OF_DAY, hours);
	    return calendar.getTime();
	}

	@Override
	public Response getWaterSupplyDetails(InputRequest inputRequest) {
		String requestId=inputRequest.getRequestId();
		logger.info("for requestId {} getWaterSupplyDetails is called",requestId);
		Long farmerId=inputRequest.getFarmerId();
		RequestData requestData=inputRequest.getRequest().getRequestData();
		Response response=new Response();
		ResponseObject responseObject=new ResponseObject();
		String status=requestData.getStatus();
		List<WaterSupplyHelper> waterSupplyDetails=new ArrayList<>();
		try {
			if(status==null || status.isEmpty())
			{
				waterSupplyDetails=waterSupplyRepository.getWaterSupplyDetailsBasedOnFarmerId(farmerId);
			}
			else
			{
				waterSupplyDetails=waterSupplyRepository.getWaterSupplyDetailsBasedOnStatus(farmerId, status);
			}
			responseObject.setWaterSupplyDetails(waterSupplyDetails);
			response.setResponseObject(responseObject);
			response.setMessage(Appconstants.SUCCESS);
			response.setStatus(Boolean.TRUE);
			response.setStatusCode(Appconstants.STATUS_200);
		} catch (Exception e) {
			logger.error("for requestId {} error occured in getWaterSupplyDetails {}",requestId,e);
		}
		
		return response;
	}

	@Override
	public Response cancelWaterSupplyDetails(InputRequest inputRequest) {
		String requestId = inputRequest.getRequestId();
		logger.info("for requestId {} cancelWaterSupplyDetails is called", requestId);
		Long farmerId = inputRequest.getFarmerId();
		RequestData requestData = inputRequest.getRequest().getRequestData();
		Response response = new Response();
		Long farmId = requestData.getFarm().getFarmId();
		try {
			WaterSupplyDetails waterSupplyDetails = waterSupplyRepository
					.findByFarmerFarmDetailsFarmFarmIdAndStatusNot(farmId, "Completed");
			if (waterSupplyDetails != null && waterSupplyDetails.getStatus().equalsIgnoreCase(Appconstants.REQUESTED)) {
				waterSupplyDetails.setModifiedBy(farmerId);
				waterSupplyDetails.setModifiedOn(LocalDateTime.now());
				waterSupplyDetails.setStatus(Appconstants.CANCELLED);
				waterSupplyDetails.setRemarks(Appconstants.CANCELLED_MSG);
				waterSupplyRepository.save(waterSupplyDetails);
				response.setMessage(Appconstants.SUCCESS);
				response.setStatus(Boolean.TRUE);
				response.setStatusCode(Appconstants.STATUS_200);
			} else if (waterSupplyDetails != null) {
				response.setMessage(Appconstants.CANCEL_NOT_ALLOWED.concat(waterSupplyDetails.getStatus()));
				response.setStatus(Boolean.FALSE);
				response.setStatusCode(Appconstants.STATUS_200);

			} else {

				response.setMessage(Appconstants.NOT_FOUND);
				response.setStatus(Boolean.FALSE);
				response.setStatusCode(Appconstants.STATUS_404);

			}
		} catch (Exception e) {
			logger.error("for requestId {} error occured in cancelWaterSupplyDetails {} ", requestId, e);

		}

		return response;
	}
}
