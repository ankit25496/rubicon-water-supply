package com.rubicon.watersupply.utils;

public class MetaDataDetails {
	
	private String firstName;
	private String lastName;
	private Long farmerId;
	private String mobileNo;
	
	public MetaDataDetails(String firstName, String lastName, Long farmerId, String mobileNo) {
	this.firstName=firstName;
	this.lastName=lastName;
	this.farmerId=farmerId;
	this.mobileNo=mobileNo;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Long getFarmerId() {
		return farmerId;
	}
	public void setFarmerId(Long farmerId) {
		this.farmerId = farmerId;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	@Override
	public String toString() {
		return "MetaDataDetails [firstName=" + firstName + ", lastName=" + lastName + ", farmerId=" + farmerId
				+ ", mobileNo=" + mobileNo + "]";
	}
}
