package com.rubicon.watersupply.utils;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Component;

@Component
public class TokenEncryptionUtils {
	public static Cipher dcipher, ecipher;

    // Responsible for setting, initializing this object's encrypter and
    // decrypter Chipher instances
	
	public TokenEncryptionUtils()
	{
		
	}
	
	public TokenEncryptionUtils(String passPhrase) {

           // 8-bytes Salt
           byte[] salt = { (byte) 0xA9, (byte) 0x9B, (byte) 0xC8, (byte) 0x32,
                        (byte) 0x56, (byte) 0x34, (byte) 0xE3, (byte) 0x03 };

           // Iteration count
           int iterationCount = 19;

           try {
                  // Generate a temporary key. In practice, you would save this key
                  // Encrypting with DES Using a Pass Phrase
                  KeySpec keySpec = new PBEKeySpec(passPhrase.toCharArray(), salt,
                               iterationCount);
                  SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES")
                               .generateSecret(keySpec);

                  ecipher = Cipher.getInstance(key.getAlgorithm());
                  dcipher = Cipher.getInstance(key.getAlgorithm());

                  // Prepare the parameters to the cipthers
                  AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt,
                               iterationCount);

                  ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);
                  dcipher.init(Cipher.DECRYPT_MODE, key, paramSpec);

           } catch (InvalidAlgorithmParameterException e) {
               e.printStackTrace();
           } catch (InvalidKeySpecException e) {
        	   e.printStackTrace();
           } catch (NoSuchPaddingException e) {
        	   e.printStackTrace();
           } catch (NoSuchAlgorithmException e) {
        	   e.printStackTrace();
           } catch (InvalidKeyException e) {
        	   e.printStackTrace();
           }
    }

    // Encrpt Password
    
    public String encrypt(String str) {
           try {
                  // Encode the string into bytes using utf-8
                  byte[] utf8 = str.getBytes("UTF8");
                  // Encrypt
                  byte[] enc = ecipher.doFinal(utf8);
                  // Encode bytes to base64 to get a string
                  return Base64.encodeBase64String(enc);

           } catch (BadPaddingException e) {
        	   e.printStackTrace();
           } catch (IllegalBlockSizeException e) {
        	   e.printStackTrace();
           } catch (UnsupportedEncodingException e) {
        	   e.printStackTrace();
           }
           return null;
    }

    // Decrpt password
    // To decrypt the encryted password
    public String decrypt(String str) {
           Cipher dcipher = null;
           try {
                  byte[] salt = { (byte) 0xA9, (byte) 0x9B, (byte) 0xC8, (byte) 0x32,
                               (byte) 0x56, (byte) 0x34, (byte) 0xE3, (byte) 0x03 };
                  int iterationCount = 19;
                  try {
                        String passPhrase = "";
                        KeySpec keySpec = new PBEKeySpec(passPhrase.toCharArray(),
                                      salt, iterationCount);
                        SecretKey key = SecretKeyFactory
                                      .getInstance("PBEWithMD5AndDES")
                                      .generateSecret(keySpec);
                        dcipher = Cipher.getInstance(key.getAlgorithm());
                        // Prepare the parameters to the cipthers
                        AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt,
                                      iterationCount);
                        dcipher.init(Cipher.DECRYPT_MODE, key, paramSpec);
                  } catch (InvalidAlgorithmParameterException e) {
                	  e.printStackTrace();
                  } catch (InvalidKeySpecException e) {
                	  e.printStackTrace();
                  } catch (NoSuchPaddingException e) {
                	  e.printStackTrace();
                  } catch (NoSuchAlgorithmException e) {
                	  e.printStackTrace();
                  } catch (InvalidKeyException e) {
                	  e.printStackTrace();
                  }
                  // Decode base64 to get bytes
                  byte[] dec = Base64.decodeBase64(str);
                  // Decrypt
                  byte[] utf8 = dcipher.doFinal(dec);
                  // Decode using utf-8
                  return new String(utf8, "UTF8");
           } catch (BadPaddingException e) {
        	   e.printStackTrace();
           } catch (IllegalBlockSizeException e) {
        	   e.printStackTrace();
           } catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
           return null;
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
       try {

              // Create encrypter/decrypter class
              TokenEncryptionUtils encrypter = new TokenEncryptionUtils("");

              // Pass the word to be Encrypted to Encrypt()
              String encrypted = encrypter.encrypt("Ankit");
              System.out.println(encrypted);
              // Pass the encrypted word to be Decrypted to Decrypt()
              String decrypted = encrypter.decrypt(encrypted);
              System.out.println(decrypted);
       } catch (Exception e) {
    	   e.printStackTrace();
       }
    }
}

